    <div id="content"> 
    

        <div id="home" class="section">
          
      <div id="home_about" class="box">
              <h2>Bienvenidos</h2>
              <p>
                  Somos el punto de partida  y de llegada del Circuito Ecoturístico. Dicho centro está administrado por el Sr. Manuel Añez y familia. Se propone  este lugar por poseer características naturales como un riachuelo, meseta, sembradíos, arboles, hierba y grandes espacios de terrenos, los cuales utilizan para brindar recreación a los visitantes.  El nombre de este centro se debe a un inmenso árbol que allí estaba plantado y que se veía desde el pueblo. Este ambiente es aprovechado para la inducción al inicio del circuito y la bienvenida.
                </p>
                
            </div>
            
            <div id="home_gallery" class="box no_mr">
                <a href="images/gallery/01-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/01.jpg" alt="image 1" /></a>
                <a href="images/gallery/02-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/02.jpg" alt="image 2" /></a>
                <a href="images/gallery/03-l.jpg" rel="lightbox[gallery]" class="no_mr"><img src="images/gallery/03.jpg" alt="image 3" /></a>
                <a href="images/gallery/04-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/04.jpg" alt="image 4" /></a>
                <a href="images/gallery/05-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/05.jpg" alt="image 5" /></a>
                <a href="images/gallery/06-l.jpg" rel="lightbox[gallery]" class="no_mr"><img src="images/gallery/06.jpg" alt="image 6" /></a>

            </div>
            
            <div class="box home_box1 color1">
              <div id="social_links">
                    <a href="#servicios"><img src="images/serv.png" alt="Servicios" /></a>
                    <div class="clear h20"></div>
                    <h3>Servicios</h3>
                </div>
            </div>
            
            <div class="box home_box1 color3">
                <div id="social_links">
                    <a href="#gallery"><img src="images/galeria.png" alt="Galería" /></a>
                    <div class="clear h20"></div>
                    <h3>Galería</h3>
                </div>
            </div>
            
            <div class="box home_box2 color5">
              <div id="social_links">
                    <a href="#reserva"><img src="images/reserv.png" alt="Facebook" /></a>
                    <div class="clear h20"></div>
                    <h3>¡Reserva Ya!</h3>
                </div>
            </div>

            <div class="box home_box1 color4 no_mr">
                <div id="social_links">
                    <a href="http://www.facebook.com/#"><img src="images/facebook.png" alt="Facebook" /></a>
                    <a href="http://www.twitter.com/#"><img src="images/twitter.png" alt="Twitter" /></a>
                    <div class="clear h20"></div>
                    <h3>Social</h3>
                </div>  
            </div>
               
        </div> <!-- END of home -->
        
        <div class="section section_with_padding" id="servicios"> 
            <h2>Servicios</h2>
            <?php
                $servicios = showALLServicios();
                // print_r($servicios);
                foreach ($servicios as $key => $value) {
                    
                    echo '<div class="img_border img_fl">
                        <img src="uploads/'.$value['imagen'].'" alt="image" />  
                    </div>';
                    echo '<div class="half right">
                        <h3>'.$value['nombre'].'</h3>
                        <p>
                            '.$value['texto'].'
                        </p>
                    </div>
                    <div class="clear h40"></div>
                    ';
                    
                }
            ?>       

            <a href="#home" class="slider_nav_btn home_btn">home</a> 
            <a href="#home" class="slider_nav_btn previous_btn">Previous</a>
            <a href="#gallery" class="slider_nav_btn next_btn">Next</a> 

        </div>

        <div class="section section_with_padding" id="gallery"> 
            <h2>Galería</h2>
            
            <?php
                $imagenes = showALLGaleria();
                foreach ($imagenes as $key => $value) {
                    
                    echo '<a href="uploads/'.$value['imagen'].'" rel="lightbox[gallery]"><img src="uploads/'.$value['imagen'].'" alt="'.$value['descripcion'].'" /></a>';

                }
            ?>

            <!-- <a href="images/gallery/01-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/01.jpg" alt="image 1" /></a>
            <a href="images/gallery/06-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/06.jpg" alt="image 6" /></a>
            <a href="images/gallery/03-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/03.jpg" alt="image 3" /></a>
            <a href="images/gallery/03-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/03.jpg" alt="image 3" /></a>
            <a href="images/gallery/04-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/04.jpg" alt="image 4" /></a>
            <a href="images/gallery/06-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/06.jpg" alt="image 6" /></a>
            <a href="images/gallery/06-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/06.jpg" alt="image 6" /></a>
            <a href="images/gallery/06-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/06.jpg" alt="image 6" /></a>
            <a href="images/gallery/06-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/06.jpg" alt="image 6" /></a>
            <a href="images/gallery/06-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/06.jpg" alt="image 6" /></a>
            <a href="images/gallery/05-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/05.jpg" alt="image 5" /></a>
            <a href="images/gallery/06-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/06.jpg" alt="image 6" /></a>
            <a href="images/gallery/06-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/06.jpg" alt="image 6" /></a>
            <a href="images/gallery/01-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/01.jpg" alt="image 1" /></a>
            <a href="images/gallery/03-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/03.jpg" alt="image 3" /></a>
            <a href="images/gallery/06-l.jpg" rel="lightbox[gallery]"><img src="images/gallery/06.jpg" alt="image 6" /></a>
            <a href="images/gallery/06-l.jpg" rel="lightbox[gallery]" class="no_mr"><img src="images/gallery/06.jpg" alt="image 6" /></a>
             -->
            <a href="#home" class="slider_nav_btn home_btn">home</a> 
            <a href="#servicios" class="slider_nav_btn previous_btn">Previous</a>
            <a href="#reserva" class="slider_nav_btn next_btn">Next</a> 

        </div>
        <div class="section section_with_padding" id="reserva">
            <?php if (isset($_GET['ok'])): ?>
                <div class="success">
                    SE HA HECHO LA RESERVACIÓN
                </div>
            <?php endif ?>
            <h2>Reservaciones</h2>
            <div class="half left">
                <h4>Formulario de Reserva</h4>
                <p>Consulta las fechas para tener disponibilidad</p>
                <div id="contact_form">
                    <form method="post" action="reservar.php">
                        <div class="left">
                            <label for="nombre">Nombre:</label>
                            <input name="nombre" type="text" class="input_field" id="nombre" maxlength="40" />
                        </div>
                        <div class="right">                           
                            <label for="email">Email:</label>
                            <input name="email" type="text" class="input_field" id="email" maxlength="40" />
                        </div>
                        <div class="left">
                            <label for="desde">Fecha Inicio:</label>
                            <input name="desde" type="text" class="input_field" id="desde" readonly="readonly" />
                        </div>
                        <div class="right">                           
                            <label for="hasta">Fecha Final:</label>
                            <input name="hasta" type="text" class="input_field" id="hasta" readonly="readonly" />
                        </div>
                        <div class="left">
                            <label for="adultos">Cantidad de Adultos:</label>
                            <input name="adultos" type="text" class="input_field" id="adultos" maxlength="2" />
                        </div>
                        <div class="right">                           
                            <label for="ninos">Cantidad de Niños:</label>
                            <input name="ninos" type="text" class="input_field" id="ninos" maxlength="2" />
                        </div>
                        
                        <div class="clear"></div>

                        <label for="telefonos">Teléfonos de Contacto:</label>
                        <input name="telefonos" type="text" class="input_field input_field_large" id="telefonos" />
                        <input type="submit" class="submit_btn" name="submit" id="submit" value="Hacer Reservación" />
                    </form>
                </div>
            </div>
            <div class="half right">
                <img src="images/reservas.jpg" alt="">
            </div>
            <a href="#home" class="slider_nav_btn home_btn">home</a> 
            <a href="#gallery" class="slider_nav_btn previous_btn">Previous</a>
            <a href="#contactos" class="slider_nav_btn next_btn">Next</a>  
        </div>
        <div class="section section_with_padding" id="contactos">
            <h2>Contactos</h2>
            <div class="half left">
              <h4>Formulario de Contacto</h4>
              <p>Comunìcate con nosotros y responderemos cualquier preginta o inquietud</p>
              <div id="contact_form">
                  <form method="post" name="contact" action="#contact">
                      <div class="left">
                          <label for="nombre">Nombre:</label>
                          <input name="nombre" type="text" class="input_field" id="nombre" maxlength="40" />
                      </div>
                      <div class="right">                           
                          <label for="email">Email:</label>
                          <input name="email" type="text" class="input_field" id="email" maxlength="40" />
                      </div>
                      <div class="clear"></div>
                      <label for="comentario">Comentario:</label>
                      <textarea id="text" name="comentario" rows="10" cols="50"></textarea>
                      <input type="submit" class="submit_btn float_l" name="submit" id="submit" value="Enviar Comentario" />
                  </form>
              </div>
            </div>

            <div class="half right">
              <h4>Dirección</h4>
              Calle Paez, Sector El Arbolón<br />
              Casa N° 79, Parroquia Jajó,<br />
              Municipio Urdaneta,<br />
              Estado Trujillo, Venezuela.<br>
              <strong>Email: anezbuscema@hotmail.com</strong><br />
              <strong>Teléfonos:</strong><br />
              <strong>Fijo: +58 271 3113593</strong><br />
                <strong>Móvil: +58 424 7135576 / 424 7023541</strong><br />
              
              <div class="clear h20"></div>
              <div class="img_nom img_border"><span></span>
              
              <iframe width="300" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=es-419&amp;geocode=&amp;q=Jaj%C3%B3,+venezuela&amp;aq=&amp;sll=36.527295,-95.009766&amp;sspn=38.902623,86.572266&amp;ie=UTF8&amp;hq=&amp;hnear=Jaj%C3%B3,+La+Mesa+de+Los+Morenos,+Venezuela&amp;t=m&amp;ll=9.079468,-70.65196&amp;spn=0.025427,0.025749&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=es-419&amp;geocode=&amp;q=Jaj%C3%B3,+venezuela&amp;aq=&amp;sll=36.527295,-95.009766&amp;sspn=38.902623,86.572266&amp;ie=UTF8&amp;hq=&amp;hnear=Jaj%C3%B3,+La+Mesa+de+Los+Morenos,+Venezuela&amp;t=m&amp;ll=9.079468,-70.65196&amp;spn=0.025427,0.025749&amp;z=14&amp;iwloc=A" style="color:#0000FF;text-align:left" target="_blank">Ver mapa más grande</a></small>
              
            </div>
            <a href="#home" class="slider_nav_btn home_btn">home</a> 
            <a href="#reserva" class="slider_nav_btn previous_btn">Previous</a>
        </div>
    </div> 
</div>