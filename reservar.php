
<?php
  include_once("admin/config.php");
  include_once("modelo-principal.php");

  function date2mySQL($date)
  {
    $fecha = explode("/", $date);
    return $fecha[2]."-".$fecha[1]."-".$fecha[0];
  }

  $data = $_POST;

  $data['desde'] = date2mySQL($data['desde']);
  $data['hasta'] = date2mySQL($data['hasta']);
  if (guardarReserva($data))
  {
    
    echo "<script>window.location='http://".$_SERVER['HTTP_HOST']."?ok#reserva'</script>";
  
  }

?>