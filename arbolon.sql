-- phpMyAdmin SQL Dump
-- version 4.0.6deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 13, 2013 at 12:11 PM
-- Server version: 5.5.34-0ubuntu0.13.10.1
-- PHP Version: 5.5.3-1ubuntu2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `arbolon`
--

-- --------------------------------------------------------

--
-- Table structure for table `galeria`
--

CREATE TABLE IF NOT EXISTS `galeria` (
  `idgaleria` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(75) NOT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`idgaleria`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `galeria`
--

INSERT INTO `galeria` (`idgaleria`, `imagen`, `descripcion`) VALUES
(4, '01.jpg', 'ImÃ¡gen de Prueba'),
(5, '02.jpg', 'ImÃ¡gen de Prueba 2'),
(6, '03.jpg', 'ImÃ¡gen de Prueba 3');

-- --------------------------------------------------------

--
-- Table structure for table `reservaciones`
--

CREATE TABLE IF NOT EXISTS `reservaciones` (
  `idreservaciones` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(75) NOT NULL,
  `desde` date NOT NULL,
  `hasta` date NOT NULL,
  `email` varchar(30) NOT NULL,
  `adultos` int(11) NOT NULL,
  `ninos` int(11) NOT NULL,
  `telefonos` varchar(75) NOT NULL,
  PRIMARY KEY (`idreservaciones`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `reservaciones`
--

INSERT INTO `reservaciones` (`idreservaciones`, `nombre`, `desde`, `hasta`, `email`, `adultos`, `ninos`, `telefonos`) VALUES
(5, 'shdjfgdskj', '2013-11-14', '2013-11-30', 'jhdgsjfhds', 2, 0, '65765456'),
(6, 'jshdgfjkds', '2013-11-14', '2013-11-30', 'sjhdfsd@sdfds.com', 2, 0, '234324324');

-- --------------------------------------------------------

--
-- Table structure for table `servicios`
--

CREATE TABLE IF NOT EXISTS `servicios` (
  `idservicios` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `imagen` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`idservicios`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `servicios`
--

INSERT INTO `servicios` (`idservicios`, `nombre`, `texto`, `imagen`) VALUES
(23, 'Ãreas de RecreaciÃ³n', 'Mesa de ping- pong, Mesas de hockey, Mesa de futbolito, Mesa de pool, Mesa de billar, Domino, ajedrez y cartas , TrampolÃ­n, Columpio , 2 Castillo Inflable , Ãrea para niÃ±os(casita de juego, parque, pared para escalar), Carrito de perro calientes, algodones,cotufas, Carros de karting, PaintBall.', 'servicios.jpg'),
(24, 'Servicio de Restaurant', 'Con comidas tÃ­picas e internacionales.', 'restaurant.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `idusuarios` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `clave` varchar(100) NOT NULL,
  PRIMARY KEY (`idusuarios`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`idusuarios`, `nombre`, `clave`) VALUES
(1, 'user', 'ee11cbb19052e40b07aac0ca060c23ee');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
