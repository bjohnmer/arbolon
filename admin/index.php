<?php
  extract($_REQUEST);  // EL EXTRACT va a pasar todas las variables y las convierte en variables php, al arreglo de variables REQUEST
  // si esta vacia o no existe la variable p muestra la pagina principal.
  if (empty($p)) 
  {
    include_once("entrada.php");  // include_once es una funcion que llama a una libreria o una pagina que va ser este caso.
  }
  else 
  {
    if (file_exists($p.".php"))
    {
      include_once($p.".php");
    }
    else
    {
      include_once("404.php");
    }
    
  }
?>