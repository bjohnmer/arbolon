<?php
  
  extract($_POST);

  if (!empty($_FILES['imagen']['name'])) {
    // Ruta donde se guardarán las imágenes
    $directorio = '../uploads/';
    // Recibo los datos de la imagen
    $nombre = $_FILES['imagen']['name'];
    $tipo = $_FILES['imagen']['type'];
    $tamano = $_FILES['imagen']['size'];

    // Muevo la imagen desde su ubicación
    // temporal al directorio definitivo
    unlink("../uploads/".$vimagen);
    @move_uploaded_file($_FILES['imagen']['tmp_name'],$directorio.$nombre);
    $imagen = $nombre;
    @chmod($directorio.$nombre, 0777);
  }
  else
  {
    $imagen = $vimagen;
  }

  $data = array(
          'nombre' => $nombre, 
          'texto' => $texto, 
          'imagen' => $imagen 
          );

  if (editarServicio($data,$id))
  {
    echo"<h3>SE HA MODIFICADO SATISFACTORIAMENTE</h3>";
    echo '<a href="servicios.php">Atrás</a>';
  }
?>