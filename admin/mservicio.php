        <?php 
          extract($_GET);
          $servicio = getById($id);
        ?>
        <div id="content"> 
    		
            <div class="section section_with_padding" id="entrada"> 
                <h2>Servicios</h2>
                <!-- <div> -->
                  <h4>Modificar Servicio</h4>
                  <div id="form" style="width:100%;">
                      <form action="servicios.php?f=modificar-servicio" method="POST" enctype="multipart/form-data">
                          <!-- <div class="left"> -->
                              <label for="nombre">Nombre:</label>
                              <input name="nombre" type="text" class="input_field" id="nombre" maxlength="255" value="<?=$servicio['nombre']?>"/>
                          <!-- </div> -->

                          <!-- <div class="right">                            -->
                              <label for="imagen">Imágen:</label>
                              <input name="imagen" type="file" class="input_field" id="imagen" maxlength="75" />
                              <img src="../uploads/<?=$servicio['imagen']?>" alt="">
                              <small>La imágen debe tener un tamaño de 360x160px</small>
                          <!-- </div> -->

                          <label for="texto">Texto:</label>
                          <textarea id="text" name="texto" rows="5"><?=$servicio['texto']?></textarea>
                          
                          <div class="clear"></div>
                          
                          <input type="submit" class="submit_btn float_l" name="submit" id="submit" value="Entrar" />
                          <input type="hidden" name="id" value="<?=$servicio['idservicios']?>">
                          <input type="hidden" name="vimagen" value="<?=$servicio['imagen']?>">
                      </form>
                  </div>
                <!-- </div> -->
                
                <a href="adminsesion.php" class="slider_nav_btn home_btn">home</a> 

            </div> <!-- END of entrada -->
        </div> <!-- END of content -->
            
