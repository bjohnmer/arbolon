    <div id="content"> 
    

        <div id="home" class="section">
          
      <div id="home_about" class="box">
            <h2>Bienvenidos al</h2>
            <h2>Área Administrativa</h2>
            <p>
                Desde aquí podrá registrar las imágenes, revisar las reservaciones y administrar los servicios del sistema.      
            </p>
                
            </div>
            
            <div id="home_gallery" class="box no_mr">
                <a href="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>images/gallery/01-l.jpg" rel="lightbox[gallery]"><img src="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>images/gallery/01.jpg" alt="image 1" /></a>
                <a href="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>images/gallery/02-l.jpg" rel="lightbox[gallery]"><img src="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>images/gallery/02.jpg" alt="image 2" /></a>
                <a href="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>images/gallery/03-l.jpg" rel="lightbox[gallery]" class="no_mr"><img src="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>images/gallery/03.jpg" alt="image 3" /></a>
                <a href="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>images/gallery/04-l.jpg" rel="lightbox[gallery]"><img src="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>images/gallery/04.jpg" alt="image 4" /></a>
                <a href="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>images/gallery/05-l.jpg" rel="lightbox[gallery]"><img src="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>images/gallery/05.jpg" alt="image 5" /></a>
                <a href="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>images/gallery/06-l.jpg" rel="lightbox[gallery]" class="no_mr"><img src="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>images/gallery/06.jpg" alt="image 6" /></a>

            </div>
            
            <div class="box home_box1 color1">
              <div id="social_links">
                    <a href="servicios.php"><img src="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>images/serv.png" alt="Servicios" /></a>
                    <div class="clear h20"></div>
                    <h3>Servicios</h3>
                </div>
            </div>
            
            <div class="box home_box1 color3">
                <div id="social_links">
                    <a href="galeria.php"><img src="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>images/galeria.png" alt="Galería" /></a>
                    <div class="clear h20"></div>
                    <h3>Galería</h3>
                </div>
            </div>
            
            <div class="box home_box2 color5">
              <div id="social_links">
                    <a href="reservaciones.php"><img src="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>images/reserv.png" alt="Facebook" /></a>
                    <div class="clear h20"></div>
                    <h3>Reservaciones</h3>
                </div>
            </div>

            <div class="box home_box1 color4 no_mr">
                <div id="social_links">
                    <a href="http://www.facebook.com/#"><img src="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>images/facebook.png" alt="Facebook" /></a>
                    <a href="http://www.twitter.com/#"><img src="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>images/twitter.png" alt="Twitter" /></a>
                    <div class="clear h20"></div>
                    <h3>Social</h3>
                </div>  
            </div>
               
        </div> <!-- END of home -->
    </div> 
</div>