        <div id="content"> 
            
            <div class="section section_with_padding" id="entrada"> 
                <h2>Reservaciones</h2>
                <p>
                    Desde aquí podrá las reservaciones hechas.      
                </p>
                <p>
                    <!-- <a class="agregar" href="servicios.php?f=nservicio">Agregar Nuevo Servicio</a> -->

                    <table summary="Datos">
                        <thead>
                            <tr>
                                <th scope="col" width="20%">Email</th>
                                <th scope="col" width="20%">Nombre</th>
                                <th scope="col" width="10%">Adultos</th>
                                <th scope="col" width="10%">Niños</th>
                                <th scope="col" width="10%">Telf</th>
                                <th scope="col" width="15%">Desde</th>
                                <th scope="col" width="15%">Hasta</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th scope="row"></th>
                                <td colspan="8"></td>
                            </tr>
                        </tfoot>
                        <tbody>
        <!-- nombre  desde   hasta   email   adultos     ninos   telefonos  -->
                        <?php
                            $reservaciones = showALL();
                            foreach ($reservaciones as $key => $value) {
                                echo "<tr>";
                                echo "<th scope='row'>".$value['email']."</th>";
                                echo "<td>".$value['nombre']."</td>";
                                echo "<td>".$value['adultos']."</td>";
                                echo "<td>".$value['niños']."</td>";
                                echo "<td>".$value['telefonos']."</td>";
                                echo "<td>".$value['desde']."</td>";
                                echo "<td>".$value['hasta']."</td>";
                                echo "<td>
                                    <a title='Eliminar' href='reservaciones.php?f=eliminar-reservacion&id=".$value['idreservaciones']."'>Eliminar</a>
                                </td>";
                                echo "</tr>";
                            }
                        ?>
                                    <!-- <a title='Modificar' href='servicios.php?f=mservicio&id=".$value['idservicios']."'>Editar</a> | -->
                        </tbody>
                    </table>
                </p>

                <a href="adminsesion.php" class="slider_nav_btn home_btn">home</a> 

            </div> <!-- END of entrada -->
        </div> <!-- END of content -->
