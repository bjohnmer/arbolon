<?php
  @session_name("arbolon"); //si esta creada la sesion obvia la funcion pero si no esta creada la crea
  @session_start();
  if (empty($_SESSION['id'])) {
    header ("location:index.php");
  }
  if (isset($_GET['s'])){  //isset me va a devolver si la variable esta creada o no
    session_destroy();
    header("location:/admin");
  }

?>