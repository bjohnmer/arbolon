        <div id="content"> 
    		
            <div class="section section_with_padding" id="entrada"> 
                <h2>Entrar</h2>
                <div class="half left">
                  <h4>Formulario de Entrada</h4>
                  <p>Área Administrativa</p>
                  <div id="contact_form">
                      <form method="post" name="contact" action="login.php">
                          <div class="left">
                              <label for="nombre">Usuario:</label>
                              <input name="usuario" type="text" class="input_field" id="usuario" maxlength="40" />
                          </div>
                          <div class="right">                           
                              <label for="clave">Clave:</label>
                              <input name="clave" type="password" class="input_field" id="clave" maxlength="40" />
                          </div>
                          <div class="clear"></div>
                          <input type="submit" class="submit_btn float_l" name="submit" id="submit" value="Entrar" />
                      </form>
                  </div>
                </div>
                <div class="img_border img_fr">
                    <img src="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>images/nosotros.jpg" alt="image" />    
                </div>

                <a href="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>index.php" class="slider_nav_btn home_btn">home</a> 

            </div> <!-- END of entrada -->
        </div> <!-- END of content -->
            
