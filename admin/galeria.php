<?php 
  include_once("pol.php");
  include_once("config.php");
  include_once("modelo-galeria.php");
  include_once("../vista-header.php");
  extract($_GET);
  if (empty($f)) {
    include_once("vista-content-galeria.php");
  }else {
    if (file_exists($f.".php")){
      include_once($f.".php");
    }else {
      include_once("404.php");
    }
  }
  
  include_once("../vista-footer.php");

?>