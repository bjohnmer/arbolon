        <div id="content"> 
            
            <div class="section section_with_padding" id="entrada"> 
                <h2>Servicios</h2>
                <p>
                    Desde aquí podrá administrar los servicios de la página.      
                </p>
                <p>
                    <a class="agregar" href="servicios.php?f=nservicio">Agregar Nuevo Servicio</a>

                    <table summary="Datos">
                        <thead>
                            <tr>
                                <th scope="col" width="10%">Id</th>
                                <th scope="col" width="35%">Nombre</th>
                                <th scope="col" width="35%">Imágen</th>
                                <th scope="col" width="20%">Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th scope="row"></th>
                                <td colspan="4"></td>
                            </tr>
                        </tfoot>
                        <tbody>
                        <?php
                            $servicios = showALL();
                            foreach ($servicios as $key => $value) {
                                echo "<tr>";
                                echo "<th scope='row'>".$value['idservicios']."</th>";
                                echo "<td>".$value['nombre']."</td>";
                                echo "<td>".$value['imagen']."</td>";
                                echo "<td>
                                    <a title='Modificar' href='servicios.php?f=mservicio&id=".$value['idservicios']."'>Editar</a> |
                                    <a title='Eliminar' href='servicios.php?f=eliminar-servicio&id=".$value['idservicios']."'>Eliminar</a>
                                </td>";
                                echo "</tr>";
                            }
                        ?>
                        </tbody>
                    </table>
                </p>

                <a href="adminsesion.php" class="slider_nav_btn home_btn">home</a> 

            </div> <!-- END of entrada -->
        </div> <!-- END of content -->
