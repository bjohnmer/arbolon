        <div id="content"> 
            
            <div class="section section_with_padding" id="entrada"> 
                <h2>Galería de Imágenes</h2>
                <p>
                    Desde aquí podrá administrar las imágenes de la página.      
                </p>
                <p>
                    <a class="agregar" href="galeria.php?f=ngaleria">Agregar Nueva Imágen</a>

                    <table summary="Datos">
                        <thead>
                            <tr>
                                <th scope="col" width="10%">Id</th>
                                <th scope="col" width="35%">Archivo</th>
                                <th scope="col" width="35%">Descripción</th>
                                <th scope="col" width="20%">Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th scope="row"></th>
                                <td colspan="4"></td>
                            </tr>
                        </tfoot>
                        <tbody>
                        <?php
                            $imagenes = showALL();
                            foreach ($imagenes as $key => $value) {
                                echo "<tr>";
                                echo "<th scope='row'>".$value['idgaleria']."</th>";
                                echo "<td>".$value['imagen']."</td>";
                                echo "<td>".$value['descripcion']."</td>";
                                echo "<td>
                                        <a title='Modificar' href='galeria.php?f=mgaleria&id=".$value['idgaleria']."'>Editar</a> |
                                        <a title='Eliminar' href='galeria.php?f=eliminar-galeria&id=".$value['idgaleria']."'>Eliminar</a>
                                    </td>";
                                echo "</tr>";
                            }
                        ?>
                        </tbody>
                    </table>
                </p>

                <a href="adminsesion.php" class="slider_nav_btn home_btn">home</a> 

            </div> <!-- END of entrada -->
        </div> <!-- END of content -->
