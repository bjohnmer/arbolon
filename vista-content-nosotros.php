        <div id="content"> 
    		
            <div class="section section_with_padding" id="nosotros"> 
                <h2>Nosotros</h2>
                
                <div class="img_border img_fl">
                    <img src="images/nosotros.jpg" alt="image" />    
                </div>
                <div id="nosotros_about" class="half right">
                    <p>
                        Somos el punto de partida  y de llegada del Circuito Ecoturístico. Dicho centro está administrado por el Sr. Manuel Añez y familia. Se propone  este lugar por poseer características naturales como un riachuelo, meseta, sembradíos, arboles, hierba y grandes espacios de terrenos, los cuales utilizan para brindar recreación a los visitantes.  El nombre de este centro se debe a un inmenso árbol que allí estaba plantado y que se veía desde el pueblo. Este ambiente es aprovechado para la inducción al inicio del circuito y la bienvenida.
                    </p>
                </div>
                
                <a href="index.php#home" class="slider_nav_btn home_btn">home</a> 
                <a href="#io" class="slider_nav_btn next_btn">Next</a> 

            </div> <!-- END of home -->
            
            <div class="section section_with_padding" id="io"> 
                <h2>Información Organizativa</h2>
                <p>
                    
                <div class="img_border img_fl">
                    <img src="images/organizativa.jpg" alt="image" />
                </div>
                <div class="half right">
                    <h3>Presidentes:</h3>
                    <p>Manuel Añez y Graciela de Añez</p>
                    <h4>Limpieza:</h4>
                    <p>Liliana Santiago</p>
                    <p>Isabel Montillo</p>
                    <h4>Chef:</h4>
                    <p>Cira Rojas </p>
                    <p>Ana Montilla</p>
                    <h4>Supervisora de personal:</h4>
                    <p>Lourdes Añez</p>
                    <h4>Supervisora de Seguridad e Higiene</h4>
                    <p>Ing. María A. Añez</p>
                    <h4>Mesoneros:</h4>
                    <p>Tatiana Briceño</p>
                    <p>Douglas Pérez</p>
                    <h4>Control Administrativo:</h4>
                    <p>María V. Añez</p>
                    <h4>Obrero de campo:</h4>
                    <p>Leonardo Rivero</p>
                </div>
                </p>
                <p>
                    
                <div class="img_border img_fl">
                    <img src="images/misionvision.jpg" alt="Misión y Visión" />
                </div>
                <div class="half right">
                    <h3>Misión:</h3>
                    <p>
                        Nuestra misión es ser un Centro Turístico reconocido por sus servicios de calidad, su continua preocupación por mejorar, respetar el medio ambiente y brindar un ambiente familiar, lo que será un sello distintivo de nuestra gestión. Además de esta forma contribuir al crecimiento de la actividad turística de la parroquica de Jajo con nuestros servicios de alojamiento y recreación.
                    </p>
                    <h3>Visión:</h3>
                    <p>
                        Mantenernos posicionados con el reconocimiento de gestión y servicios de nuestros clientes; mediante la garantía de confiabilidad, respeto, pro actividad y dinamismo. 
                    </p>
                    <p>
                        Mantener la confianza y amistad de nuestros clientes, estableciendo relaciones comerciales a largo plazo. Posicionarnos en el corazón de su organización; y convertirnos en su aliado.
                    </p>
                </div>
                </p>
                 
                <a href="index.php#home" class="slider_nav_btn home_btn">home</a> 
                <a href="#nosotros" class="slider_nav_btn previous_btn">Previous</a>
                <a href="#jajo" class="slider_nav_btn next_btn">Next</a> 

            </div>

            <div class="section section_with_padding" id="jajo"> 
                <h2>Jajó</h2>
                
                <div class="img_border img_fl">
                    <img src="images/jajo1.jpg" alt="image" />    
                </div>
                <div class="half right">
                    <p>
                        Situado en la vía que conduce de Trujillo a Mérida, tomando un desvío que se encuentra cerca de Timotes, se encuentra uno de los pueblos más bellos del Estado Trujillo, y quizás de toda Venezuela: Jajó.
                    </p>
                </div>
                
                <div class="clear h20"></div>

                <div class="img_border img_fl">
                    <img src="images/jajo2.jpg" alt="image" />    
                </div>
                <div class="half right">
                    <p>
                        El encanto de Jajó empieza mucho antes de llegar. Los 20 minutos de carretera pueden incrementarse muy fácilmente si uno cae en la tentación de detenerse para admirar el paisaje: árboles espectaculares, montañas majestuosas, campos cultivados.
                    </p>
                </div>
                
                <div class="clear h20"></div>

                <div class="img_border img_fl">
                    <img src="images/jajo3.jpg" alt="image" />    
                </div>
                <div class="half right">
                    <p>
                        Como en casi todos los pueblos Venezolanos, la vida se da alrededor de la Plaza Bolívar, frente a la cual se encuentra la iglesia. Sin embargo, si Ud. va a Jajó, disfrutará mucho la caminata por el pueblo.
                    </p>
                </div>
                                
                <a href="index.php#home" class="slider_nav_btn home_btn">home</a> 
                <a href="#io" class="slider_nav_btn previous_btn">Previous</a>

            </div>
            
        </div> 
    </div>