<!doctype html>
<html lang="es">
    <head> 
        <meta charset="UTF-8" />
        <title>Centro Turìstico Familiar El Arbolón</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <link href="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>css/templatemo_style.css" type="text/css" rel="stylesheet" /> 
        
        <script type="text/javascript" src="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>js/jquery-1.9.1.js"></script> 
        <script type="text/javascript" src="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>js/jquery.scrollTo-min.js"></script> 
        <script type="text/javascript" src="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>js/jquery.localscroll-min.js"></script> 
        <script type="text/javascript" src="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>js/init.js"></script>
        <!--  
        <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.js"></script>
        -->
        
        <link rel="stylesheet" href="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>css/le-frog/jquery-ui-1.10.3.custom.min.css" type="text/css" media="screen">
        <script type="text/javascript" src="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>js/jquery-ui-1.10.3.custom.min.js"></script>
        <script>
            $(document).ready(function(){
                $("#desde, #hasta").datepicker({
                    dateFormat: "dd/mm/yy",
                    minDate: "0",
                    dayNames: ['Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo'],
                    dayNamesMin: ['Lu','Ma','Mi','Ju','Vi','Sá','Do'],
                    dayNameShort: ['Lun','Mar','Mié','Jue','Vie','Sáb','Dom'],
                    monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
                    monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic']
                }); 
            });
        </script>
        <link rel="stylesheet" href="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>css/slimbox2.css" type="text/css" media="screen" /> 
        <script type="text/JavaScript" src="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>js/slimbox2.js"></script> 

        <link rel="stylesheet" href="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>css/estilos.css" type="text/css" media="screen" /> 

    </head> 
    <body>
    
    <div id="templatemo_header">
        <div id="site_title">
            <a href="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>" title="Centro Turístico Familiar El Arbolón">
                <h1>
                    <img src="<?='http://'.$_SERVER['HTTP_HOST']."/arbolon/"?>images/logo.png" alt="Centro Turístico Familiar El Arbolón">
                </h1>
            </a>
            <?php if (!empty($_SESSION['id'])): ?>
            <ul class="menu">
                <li>
                    <a href="index.php?p=adminsesion">Menú Principal</a>
                </li>
                <li>
                    <strong>Usuario: 
                    <?php 
                        echo $_SESSION['nombre'];
                    ?>
                    </strong>
                </li>
                <li>
                    <a href="pol.php?s">salir</a>
                </li>
            </ul>
            <?php else: ?>
            <ul class="menu">
                <li><a href="index.php?p=nosotros#nosotros">Nosotros</a></li>
                <li><a href="index.php?p=nosotros#io">Información Organizacional</a></li>
                <li><a href="index.php?p=nosotros#jajo">Jajó</a></li>
                <li><a href="index.php#contactos">Contactos</a></li>
            </ul>
            <?php endif ?>

        </div>
    </div>
    <div id="templatemo_main">
